import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Recipies from './components/Recipies';
import Axios from 'axios';

function App() {
  const[search,setSearch]=useState("chicken");
  const[recipies,setRecipies]=useState([]);

  const APP_ID="3a44556d";
  const APP_KEY="2e705423e49a1174864ce991a61bf119";

  useEffect( ()=>{
    getRecipies();
    
  },[]);
  const getRecipies= async()=>{
    const res = await Axios.get(`https://api.edamam.com/search?q=${search}&app_id=${APP_ID}&app_key=${APP_KEY}`);
    
    setRecipies(res.data.hits);
  }

  
  const onInputChange=e=>{
    setSearch(e.target.value);
  };
  const onSearchClick=()=>{
    getRecipies();
  }
  return (
    <div className="App">
    <h1 className="tc">Food Recipies Website</h1>
    <Header search={search} onInputChange={onInputChange} onSearchClick={onSearchClick}/>
    <div className="container">
    <Recipies recipies={recipies}/>
    </div>
    </div>
  );
}

export default App;
