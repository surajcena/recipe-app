import React from 'react';
import ReactDom from 'react-dom';
import './Avatar.css';
import 'tachyons';


const Avatar=(props)=>{
    return(
        <div className="best ma4 bg-light-purple dib tc grow shadow-4">
            <img src={'https://joeschmoe.io/api/v1/${props.name}'} alt="avatar"/>
            <h1>{props.name}</h1>
            <p>{props.position}</p>
            
        </div>
    )
}
export default Avatar;