import React from 'react';
import Recipieitem from './Recipieitem';
const Recipies=(props)=>{
    const{recipies}=props;
    return (
        <div className="card-columns">
        
       { recipies.map(recipe=>(
       <Recipieitem name={recipe.recipe.label}
                    image={recipe.recipe.image}
                    ingredientLines={recipe.recipe.ingredientLines}/>
          
        
        ))
       }
            </div>
    );
};
export default Recipies;