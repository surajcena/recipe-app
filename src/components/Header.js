import React from 'react';

const Header=(props)=>{
    const{search,onInputChange,onSearchClick}=props
    // tala props.somthing easy garauna
    return(
        <div className="jumbotron py-4">
     
            <h1 className="display-1">
            <span class="material-icons brand-icon">
                 fastfood
                 </span>
             Food Recipe</h1>
             <div class="input-group w-50 mx-auto">
  <input type="text" class="form-control" aria-label="Recipient's username" placeholder="search your recipe" aria-describedby="basic-addon2"
     value={search}
    onChange={onInputChange}/>
  <div class="input-group-append">
    <button className="bbtn btn-dark" onClick={onSearchClick}>Search Recipe</button>
  </div>
</div>
        </div>
        
    )
}
export default Header;