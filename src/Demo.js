import React,{Component} from 'react';
import ReactDom from 'react-dom';
import './Demo.css';
import Avatar from './Avatar';
 class Demo extends Component{

    constructor(){
         super();
         this.state= {
             name:"welcome guys"
         }
     }
     namechange(){
         this.setState(
             {
                 name:"i would like to welcome you to my site"
             }
         )
     }
  render(){

        const arraylist=[
            {
            name:"suraj",
            position:"software_developer",
            },
            {
              name:"prashant",
              position:"student",
            },
            {
                name:"sushmita",
                position:"nurse",
              }

        ]
       const hi=arraylist.map((me,i)=>{
            return<Avatar key={i} name={arraylist[i].name}
                                    position={arraylist[i].position}/>

        })

        return(
            <div>
            <h1>{this.state.name}</h1>
            <div className="test">

                {hi}

            </div>
            <button onClick={()=>this.namechange()} >click me</button>
            </div>
        )

}
}
export default Demo;
